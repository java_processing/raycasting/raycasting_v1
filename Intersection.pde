class Intersection extends PVector {
  
  private boolean exists = false;
  
  Intersection(PVector dirRayon, Segment sgmt) {
    float[][] arr2x2 = new float[2][2];
    arr2x2[0][0] = sgmt.fin.x - sgmt.debut.x;
    arr2x2[0][1] = -dirRayon.x;
    arr2x2[1][0] = sgmt.fin.y - sgmt.debut.y;
    arr2x2[1][1] = -dirRayon.y;
    
    float[][] arr2x1 = new float[2][1];;
    arr2x1[0][0] = -sgmt.debut.x;
    arr2x1[1][0] = -sgmt.debut.y;
    
    Matrix mat = Matrix.array(arr2x2);
    Matrix vct = Matrix.array(arr2x1);
    
    float[][] solu = (solve(mat, vct)).array;
    float[] sol = {solu[0][0], solu[1][0]};
    
    if ((sol[0]>=-0 && sol[0]<=1) && (sol[1]>=0)) {
      exists = true;
      this.x = sgmt.debut.x+sol[0]*arr2x2[0][0];
      this.y = sgmt.debut.y+sol[0]*arr2x2[1][0];
    }  
  }
  
  public boolean exists() {
    return exists;
  }
}
