class MultiSource extends RaySource{
  
  ArrayList<RaySource> sources = new ArrayList<RaySource>();
  float rayon;
  int nbSources;
  float agl;
  
  MultiSource(int nb, float r) {
    super();
    this.rayon = r;
    this.nbSources = nb;
    if (this.nbSources>0) {
      this.agl = TWO_PI/this.nbSources;
      for (int i=0 ; i<this.nbSources ; i++) {
        this.sources.add(new RaySource());
      }
    }
  }
  
  public void update(float x, float y) {
    this.x = x;
    this.y = y;
    
    float decx, decy;
    for (int i=0 ; i<this.nbSources ; i++) {
      decx = this.rayon*cos(i*agl);
      decy = this.rayon*sin(i*agl);
      sources.get(i).update(this.x+decx, this.y+decy);
    }
  }
  
  public void updateMouse() {
    this.update(mouseX - width/2, -mouseY + height/2);
  }
  
  public Polygon[] getArrayPolyVision() { //PGraphics img
    Polygon[] poly = new Polygon[this.nbSources+1];
    for (int i=0 ; i<this.nbSources ; i++) {
      poly[i] = sources.get(i).getPolyVision();
      //sources.get(i).showVision(img, this.nbSources, 0.75);
    }
    poly[this.nbSources] = super.getPolyVision();
    return poly;
    //super.showVision(img, 0, 1.);     
  }
  
  public void showSrc() {
    super.showSrc();
    for (int i=0 ; i<this.nbSources ; i++) {
      sources.get(i).showSrc();
    }
  }
  
  public void updateRays(Polygon[] poly) {
    super.updateRays(poly);
    for (int i=0 ; i<this.nbSources ; i++) {
      sources.get(i).updateRays(poly);
    }
  }
  
  public ArrayList<RaySource> getSources() {
    return this.sources;
  }  
}
