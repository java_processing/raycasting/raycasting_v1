class RaySource extends PVector {
  
  ArrayList<Ray> rays = new ArrayList<Ray>();
    
  RaySource() {
    super(0, 0);
  }
  
  public void update(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  public void updateMouse() {
    this.update(mouseX - width/2, -mouseY + height/2);
  }
  
  public void showSrc() {
    push();
    stroke(126);
    point(this.x, this.y);
    pop();
  }
  
  public Polygon getPolyVision() { //PGraphics img, int nbSources, float tweakTint
    Collections.sort(this.rays);
    int size = this.rays.size();
    
    //msk.translate(width/2, height/2);
    //msk.scale(1, -1);
    //msk.translate(this.x, this.y);
    PShape shp = createShape();
    shp.beginShape();
    for (int i=0 ; i<size ; i++) {
      shp.vertex(this.rays.get(i).nearestIntersect.x, this.rays.get(i).nearestIntersect.y);
      //msk.vertex(width/2 + this.x + this.rays.get(i).nearestIntersect.x, height/2 - this.y - this.rays.get(i).nearestIntersect.y);
    }
    shp.endShape(CLOSE);
    
    return new Polygon(this.x, this.y, shp);
    //img.mask(msk);
    
    //push();
    //img.mask(msk);
    ////PImage copy = img.copy();
    //int a, r, g, b, argb;
    //for (int i=0 ; i<img.pixels.length ; i++) {
    //  argb = img.pixels[i];
    //  a = (argb >> 24) & 0xFF;
    //  r = (argb >> 16) & 0xFF;  // Faster way of getting red(argb)
    //  g = (argb >> 8) & 0xFF;   // Faster way of getting green(argb)
    //  b = argb & 0xFF;          // Faster way of getting blue(argb)
    //  img.pixels[i] = color(r, g, b, a/(tweakTint*(nbSources+1)));
    //}    
    //image(img, 0, 0);
    //pop();
        
    //push();
    //img.mask(msk);
    //tint(255, 255/(tweakTint*(nbSources+1)));
    //image(img, 0, 0);
    //pop();
  }
  
  public void updateRays(Polygon[] poly) {
    
    rays.clear();    
    PShape s;
    Ray ray;
    PVector[] vt = new PVector[3];
    int nbVtx;
    
    for (Polygon pol : poly) {
      s = pol.s;
      nbVtx = s.getVertexCount();
      for (int i=0 ; i<nbVtx ; i++) {
        vt[0] = s.getVertex((i-1+nbVtx)%nbVtx).add(pol).sub(this);
        vt[1] = s.getVertex(i).add(pol).sub(this);
        vt[2] = s.getVertex((i+1)%nbVtx).add(pol).sub(this);
        ray = new Ray(this, vt, poly);
        this.rays.add(ray);
        
        if (ray.canContinue) {
          this.rays.add(ray.continued);
        }
      }
    }
  }
  
  public ArrayList<Ray> getRays() {
    return this.rays;
  }  
}
