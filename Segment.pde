class Segment implements Cloneable {
  PVector debut;
  PVector fin;
  
  Segment(PVector db, PVector fn) {
    this.debut = db;
    this.fin = fn;
  }
  
  public void add(PVector vct) {
    this.debut.add(vct);
    this.fin.add(vct);
  }
  
  public void sub(PVector vct) {
    this.debut.sub(vct);
    this.fin.sub(vct);
  }
  
  @Override
  public Segment clone() {
    return new Segment(this.debut.copy(), this.fin.copy());
  }
}
