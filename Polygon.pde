class Polygon extends PVector{

  PShape s;
  Segment[] segments;
  int nbV;
  
  Polygon(float x, float y, PShape s) {
     super(x, y);
     this.s = s;
     this.nbV = this.s.getVertexCount();
     this.segments = new Segment[this.nbV];
     
     for (int i=0 ; i<nbV ; i++) {
       segments[i] = new Segment(this.s.getVertex(i), this.s.getVertex((i+1)%nbV));
     }
  }
  
  
  
  public Segment[] getAbsSegments() {
    Segment[] sgmts = new Segment[segments.length];
    for (int i=0 ; i<segments.length ; i++) {
      sgmts[i] = segments[i].clone();
      sgmts[i].add((PVector) this);
    }
    return sgmts;
  }  
}
