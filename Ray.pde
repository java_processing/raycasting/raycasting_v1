class Ray extends PVector implements Cloneable, Comparable<Ray>{
  
  ArrayList<PVector> intersects = new ArrayList<PVector>();
  PVector nearestIntersect;
  boolean canContinue = false;
  Ray continued;
  EnumSide side = EnumSide.Mid;
  
  Ray(Ray ray) {
    super(ray.x, ray.y);
    this.intersects = ray.intersects;
    this.nearestIntersect = ray.nearestIntersect;
    canContinue = true;
  }
  
  Ray(RaySource src, PVector[] vt, Polygon[] poly) {
    super(vt[1].x, vt[1].y);
    this.nearestIntersect = (PVector) this;
    intersects.add(nearestIntersect);
    this.intersections(poly, src);
    
    PVector vct0 = new PVector();
    PVector.cross(this, vt[0], vct0);
    
    PVector vct2 = new PVector();
    PVector.cross(this, vt[2], vct2);
    
    if (vct0.z*vct2.z>=0) {
      canContinue = true;
      continued = new Ray(this);
      continued.changeNearest();
      if (vct0.z>=0 && vct2.z>=0) {
        this.side = EnumSide.Left;
        continued.side = EnumSide.Right;
      } else {
        this.side = EnumSide.Right;
        continued.side = EnumSide.Left;
      }
    }
  }
  
  private void intersections(Polygon[] poly, RaySource src) {
    Intersection intersect;
    for (Polygon pol : poly) {
      Segment[] segments = pol.getAbsSegments();
      for (Segment sgmt : segments) {
        sgmt.sub(src);
        intersect = new Intersection((PVector) this, sgmt);
        if (intersect.exists()) {
          this.intersects.add((PVector) intersect);
          if (intersect.mag()<this.nearestIntersect.mag()) {
            this.nearestIntersect = (PVector) intersect;
          }
        }
      }
    }
  }
  
  private void changeNearest() {
    if (abs(this.nearestIntersect.mag()-this.mag())<2.) {
      ListIterator<PVector> iter = intersects.listIterator();
      PVector vct = new PVector();
      this.nearestIntersect = new PVector(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
      while (iter.hasNext()) {
        vct = iter.next();
        if (vct.mag()<this.nearestIntersect.mag() && abs(vct.mag()-this.mag())>2.) {
          this.nearestIntersect = vct;   
        }
      }
    }
  }
  
  public void show() {    
    line(0, 0, this.nearestIntersect.x, this.nearestIntersect.y);
  }
  
  public int compareTo(Ray ray) {
    int signThis = (0>this.y)?-1:1;
    int signRay = (0>ray.y)?-1:1;
    float diffAgl = signThis*PVector.angleBetween(new PVector(1, 0), this) - signRay*PVector.angleBetween(new PVector(1, 0), ray);
    if (abs(diffAgl)<=0.0001 && canContinue) {
      return new Float(this.side.value).compareTo(ray.side.value);
    } else if (abs(diffAgl)<=0.0001 && !canContinue) {
      return 0;
    } else {
      return (0>diffAgl)?-1:1; 
    }
  }
}
