# Ray Casting algorithm

Ray casting algorithm, using several sources and different light intensity to create a shadow effect.
With 10 light sources and 5 polygons on the screen, i've got approximately 16 fps.
The implementation isn't really effective but it works.

Source and documents i used :
* Daniel Shiffman's video : [# Coding Challenge #145: 2D Raycasting](https://www.youtube.com/watch?v=TOEi6T2mtHo)
* [SIGHT & LIGHT by Nicky Case](https://ncase.me/sight-and-light/)
* [2d Visibility from Red Blob Games](https://www.redblobgames.com/articles/visibility/)
* [Line–line intersection](https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection)
* [Processing mask() function](https://processing.org/reference/PImage_mask_.html)
