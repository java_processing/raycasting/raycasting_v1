import pallav.Matrix.*;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Collections;



int nbPoly = 4;
Polygon[] polygons = new Polygon[nbPoly+1];
int nbSrc = 4;
float r = 15.;
int strokeWeight = 5;
int nbMultiSrc = 2;
MultiSource[] sources = new MultiSource[2];
Polygon[][] vision = new Polygon[2][nbSrc+1];



PGraphics bckGrnd, shinedImg; 



void setup() {
  frameRate(Float.POSITIVE_INFINITY); //Float.POSITIVE_INFINITY
  size(900, 600);
  
  sources[0] = new MultiSource(nbSrc, r);
  sources[1] = new MultiSource(nbSrc, r);
  
  PShape shp = createShape();
  shp = createShape();
  shp.beginShape();
  shp.noFill();
  shp.noStroke();
  shp.vertex(-width/2, -height/2);
  shp.vertex(width/2, -height/2);
  shp.vertex(width/2, height/2);
  shp.vertex(-width/2, height/2);
  shp.endShape(CLOSE);  
  polygons[0] = new Polygon(0, 0, shp);
  
  float side = 130;  
  shp = createShape();
  shp.beginShape();
  shp.noFill();
  shp.stroke(255, 255, 110);
  shp.strokeWeight(strokeWeight);
  shp.strokeJoin(ROUND);
  //shp.noStroke();
  shp.vertex(0, 0);
  shp.vertex(side, 0);
  shp.vertex(side, side);
  shp.vertex(0,side);
  shp.endShape(CLOSE);
  polygons[1] = new Polygon(100, 15, shp);
  
  side = 130;  
  shp = createShape();
  shp.beginShape();
  shp.noFill();
  shp.stroke(255, 255, 110);
  shp.strokeWeight(strokeWeight);
  shp.strokeJoin(ROUND);
  //shp.noStroke();
  shp.vertex(0, 0);
  shp.vertex(side-10, -10);
  shp.vertex(10,side-10);
  shp.endShape(CLOSE);  
  polygons[2] = new Polygon(-200, -175, shp);
  
  side = 130;
  int nbPnt = 8;
  shp = createShape(); 
  shp.beginShape();
  shp.noFill();
  shp.stroke(255, 255, 110);
  shp.strokeWeight(strokeWeight);
  shp.strokeJoin(ROUND);
  //shp.noStroke();
  for (float agl=0 ; agl<TWO_PI ; agl+=TWO_PI/nbPnt) {
    shp.vertex(cos(agl)*side/2, sin(agl)*side/2);
  }
  shp.endShape(CLOSE);
  polygons[3] = new Polygon(-200, 100, shp);
  
  side = 130;
  nbPnt = 5;
  shp = createShape(); 
  shp.beginShape();
  shp.noFill();
  shp.stroke(255, 255, 110);
  shp.strokeWeight(strokeWeight);
  shp.strokeJoin(ROUND);
  //shp.noStroke();
  for (float agl=0 ; agl<TWO_PI ; agl+=TWO_PI/nbPnt) {
    shp.vertex(cos(agl)*side/2, sin(agl)*side/2);
  }
  shp.endShape(CLOSE);
  polygons[4] = new Polygon(125, -125, shp);
  
  
  
  bckGrnd = createGraphics(width, height);
  bckGrnd.beginDraw();
  bckGrnd.background(20, 20, 20);
  bckGrnd.translate(bckGrnd.width/2, bckGrnd.height/2);
  bckGrnd.scale(1, -1);
  bckGrnd.rectMode(CENTER);
  bckGrnd.push();
  bckGrnd.translate(-50, -50);
  bckGrnd.fill(255, 10, 10, 210);
  bckGrnd.rect(0, 0, 60, 40);
  bckGrnd.pop();
  drawPoly(polygons, bckGrnd, color(20, 20, 20));
  bckGrnd.endDraw();
  
  shinedImg = createGraphics(width, height);
  shinedImg.beginDraw();
  shinedImg.background(170, 205, 200);
  shinedImg.translate(bckGrnd.width/2, bckGrnd.height/2);
  shinedImg.scale(1, -1);
  shinedImg.rectMode(CENTER);
  shinedImg.push();
  shinedImg.translate(-50, -50);
  shinedImg.fill(100, 50, 255, 190);
  shinedImg.noStroke();
  shinedImg.ellipse(0, 0, 50, 50);
  shinedImg.pop();
  drawPoly(polygons, shinedImg, color(255, 255, 110));
  shinedImg.endDraw();
}



void drawPoly(Polygon[] poly, PGraphics img, color stroke) {
  for (int i=1 ; i<nbPoly+1 ; i++) {
    img.push();
    img.translate(poly[i].x, poly[i].y);
    PShape shp = poly[i].s;
    shp.setStroke(stroke);
    img.shape(shp);
    img.pop();
  }
}



void draw() {
  if (frameCount%200==0) {
    println(frameRate);
  }
  
  image(bckGrnd, 0, 0);  
  
  sources[0].updateMouse();
  if (nbMultiSrc>1) {
    sources[1].update(-sources[0].x, -sources[0].y);
  }
  
  for (int i=0 ; i<nbMultiSrc ; i++) {
    sources[i].updateRays(polygons);
  }
  
  for (int i=0 ; i<nbMultiSrc ; i++) {
    vision[i] = sources[i].getArrayPolyVision();
  }
  drawVision(vision, shinedImg);
  
  for (int i=0 ; i<nbMultiSrc ; i++) {
    drawSource(sources[i]);
  }
}



void drawVision(Polygon[][] polyVision, PGraphics img) {
  PImage msk;
  Polygon ply;
  float tweakTint = 0.75;
  for (int i=0 ; i<nbSrc ; i++) {
    for (int j=0 ; j<nbMultiSrc ; j++) {
      ply = polyVision[j][i];
      msk = mskFromPoly(ply);
      
      img.mask(msk);
      push();
      tint(255, 255/(tweakTint*(nbSrc+1)));
      image(img, 0, 0);
      pop();
    }
  }
  for (int j=0 ; j<nbMultiSrc ; j++) {
    ply = polyVision[j][nbSrc];
    msk = mskFromPoly(ply);
    img.mask(msk);
    image(img, 0, 0);
  }
}



PImage mskFromPoly(Polygon poly) {
  PGraphics msk;
  msk = createGraphics(width, height);
  msk.beginDraw();
  msk.translate(width/2, height/2);
  msk.scale(1, -1);
  msk.translate(poly.x, poly.y);
  msk.shape(poly.s);
  msk.endDraw();
  return msk;
}



void drawSource(MultiSource src) {
  push();
  translate(width/2, height/2);
  scale(1, -1);
  stroke(50);
  strokeWeight(5);
  src.showSrc();
  pop();
}



Matrix solve(Matrix A, Matrix B) {    //find X such as A*X=B
  return Matrix.Multiply(Matrix.inverse(A), B);
}
