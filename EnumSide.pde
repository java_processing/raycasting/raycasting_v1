enum EnumSide {

  Left(1.), Right(-1.), Mid(0.);
  
  public float value;
  
  private EnumSide(float val) {
    this.value = val;
  }

}
